import "./Navigation.css";
import { NavLink } from "react-router-dom";

function Navigation() {
  return (
    <ul className="Navigation">
      {/*  ajout de Navlinks pour accéder aux différentes pages */}
      <li> <NavLink to="/">Home</NavLink> </li>
      <li> <NavLink to="/products">Products</NavLink>  </li>
      <li> <NavLink to="/cart">Cart</NavLink> </li>
    </ul>
  );
}

export default Navigation;
