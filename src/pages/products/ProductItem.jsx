import PropTypes from "prop-types";

function ProductItem(props) {
  const { title, description } = props;
  return (
    <li className="ProductItem">
      <h3>{title}</h3>
      <p>{description}</p>
      <button type="button">Add to cart</button>
    </li>
  );
}

ProductItem.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default ProductItem;
