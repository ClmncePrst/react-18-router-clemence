import React from "react";
import ReactDOM from "react-dom/client";
// TODO install and import react router
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

// création d'un router avec les différents chemins nécessaires pour chaque accéder à chaque composant
const router = createBrowserRouter(
  [
    {
      path: "/",
      element: <Home />
    },
    {
      path: "/products",
      element: <ProductList />
    },
    {
      path: "/cart",
      element: <Cart />
    },

  ]
)
ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* Ajout du react router */}
    <RouterProvider router={router} />
  </React.StrictMode>
);
